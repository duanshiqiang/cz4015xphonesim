package exception;

public class UserNotFoundAtBaseStationException extends RuntimeException {

	public UserNotFoundAtBaseStationException() {
		super();
	}

	public UserNotFoundAtBaseStationException(String message) {
		super(message);
	}
	
}
