package exception;

public class NoFreeChannelException extends Exception {
	public NoFreeChannelException(String message) {
		super(message);
	}
	public NoFreeChannelException() {
		super();
	}
}
