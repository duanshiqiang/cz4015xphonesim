package com.duanshiqiang.XphoneSim;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class Main {
	public RoadEnv roadEnv;
	
	public Main(String xmlFilePath, int baseStationReservedChannleNo) {
		this.roadEnv = new RoadEnv(baseStationReservedChannleNo);
		try {
			this.roadEnv.loadExcelData(new File(xmlFilePath));
		} catch (InvalidFormatException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public static void main(String[] args) {
//		Main.main1(args);
//		Main.main2(new String[]{"0"});
		Main.main3(new String[]{"1", "500"});
	}
	
	/**
	 * This is simulation without warm up
	 * @param args
	 */
	public static void main1(String[] args) {
		Main sim = new Main("/home/duan/Notes/CZ4015/Assignment/PCS_TEST_DETERMINSTIC_S21314.xls", 0);
		CallEventHandler handler = new CallEventHandler();
		sim.roadEnv.runsim(handler);
		System.out.println("Block call rate: " + Double.toString((double)sim.roadEnv.blockcallNo/(double)sim.roadEnv.callNo));
		System.out.println("Drop call rate: " + Double.toString((double)sim.roadEnv.dropcallNo/(double)sim.roadEnv.callNo));
	}
	
	/**
	 * This is for evaluate warm up period
	 * @param args
	 */
	public static void main2(String[] args) {
		int reserveChannelNo = Integer.parseInt(args[0]);
		Main sim = new Main("/home/duan/Notes/CZ4015/Assignment/PCS_TEST_DETERMINSTIC_S21314.xls", reserveChannelNo);
		CallEventHandler handler = new CallEventHandler();
		
		Workbook wb = new HSSFWorkbook();
		Sheet sheet = wb.createSheet();
		
		int rowIndex=1;
		Row row = null;
		row = sheet.createRow(0);
		row.createCell(0).setCellValue("Time");
		row.createCell(1).setCellValue("Channel Usage");
		
		while(sim.roadEnv.step(handler)) {
			row = sheet.createRow(rowIndex);
			rowIndex++;
			row.createCell(0).setCellValue(sim.roadEnv.getCurrentTime());
			row.createCell(1).setCellValue(sim.roadEnv.getChannelUsagePercentage());
		}
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream("/home/duan/Notes/CZ4015/Assignment/workbook.xls");
			wb.write(fileOut);
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This is simulation with warm up 
	 * @param args
	 */
	public static void main3(String[] args) {
		int reserveChannelNo = Integer.parseInt(args[0]);
		CallEventHandler handler = new CallEventHandler();
		double threshold = Double.parseDouble(args[1]);
		boolean reset;
		Main sim = null;
		Workbook wb = new HSSFWorkbook();
		Sheet sheet = wb.createSheet();
		Row row = null;
		
		for(int i=0; i<100; i++) {
			sim = new Main("/home/duan/Notes/CZ4015/Assignment/PCS_TEST_DETERMINSTIC_S21314.xls", reserveChannelNo);
			reset = false;
			while(sim.roadEnv.step(handler)) {
				if(!reset){
					if(sim.roadEnv.getCurrentTime()>threshold) {
						//reset all data collected
						sim.roadEnv.callNo = 0;
						sim.roadEnv.blockcallNo = 0;
						sim.roadEnv.callNo = 0;
						reset = true;
					}
				}
			}
			row = sheet.createRow(i);
			row.createCell(0).setCellValue((double)sim.roadEnv.blockcallNo/(double)sim.roadEnv.callNo);
			row.createCell(1).setCellValue((double)sim.roadEnv.dropcallNo/(double)sim.roadEnv.callNo);
		}
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream("/home/duan/Notes/CZ4015/Assignment/result.xls");
			wb.write(fileOut);
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
