package com.duanshiqiang.XphoneSim;

import java.util.ArrayList;

import exception.NoFreeChannelException;
import exception.UserNotFoundAtBaseStationException;

public class BaseStation {
	private ArrayList<Channel> channels;
	private int reserveNo = 0;
	
	public BaseStation() {
		this.channels = new ArrayList<Channel>(10);
		for(int i=0; i<10; i++) {
			this.channels.add(new Channel());
		}
	}
	
	public BaseStation(int reserveNo) {
		this();
		this.reserveNo = reserveNo;
	}
	
	public Channel getChannel(int index) {
		return this.channels.get(index);
	}
	
	public ArrayList<Channel> getChannels() {
		return channels;
	}
	
	private int getFreeChannelNo() {
		int i=0;
		for(Channel cn: this.channels) {
			if(cn.isFree())
				i++;
		}
		return i;
	}
	
	private Channel getOneFreeChannel() {
		for(Channel cn: this.channels) {
			if(cn.isFree())
				return cn;
		}
		return null;
	}
	
	public void allocateUser(CallOnCar user, boolean isNewUser) throws NoFreeChannelException {
		int freechannelNo = this.getFreeChannelNo();
		if(freechannelNo ==0 || (isNewUser && (freechannelNo <= this.reserveNo)) ) {
			throw(new NoFreeChannelException());
		}
		Channel cn = this.getOneFreeChannel();
		cn.setUser(user);
		cn.setFree(false);
	}
	
	public void removeUser(CallOnCar user) throws UserNotFoundAtBaseStationException{
		Channel channel = null;
		for(Channel cn: this.channels) {
			if(cn.getUser() == user) {
				channel = cn;
				break;
			}
		}
		if(channel == null) {
			throw(new UserNotFoundAtBaseStationException());
		}
		channel.setUser(null);
		channel.setFree(true);
	}
}
