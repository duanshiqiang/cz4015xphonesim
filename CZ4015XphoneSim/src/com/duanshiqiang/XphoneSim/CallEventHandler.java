package com.duanshiqiang.XphoneSim;

import java.util.PriorityQueue;

import exception.NoFreeChannelException;

public class CallEventHandler {
	public CallEventHandler() {}
	
	public void handle(RoadEnv env, PriorityQueue<CallEvent> eventQueue, CallEvent event) {
		CallOnCar user = event.getUser();
		double handoverTime;
		double terminateTime;
		
		if(event.getType() == CallEvent.TYPE.DISPATCHE) {
			
			try {
				env.getStation(user.getCurrentBaseStation()).
					allocateUser(user, true);
				//Then add the most recent handover or terminate event to the eventqueue
				handoverTime = (2. - user.getCurrentLocation()) / user.getVelocity() * 3600. + env.getCurrentTime();
				terminateTime = user.getArrivalTime() + user.getCallDuration();
				
				if( terminateTime < handoverTime ) {
					eventQueue.add(new CallEvent(CallEvent.TYPE.TERMINATION, user, terminateTime));
				}else {
					eventQueue.add(new CallEvent(CallEvent.TYPE.HANDOVER, user, handoverTime));
				}
				env.callNo++;
			} catch (NoFreeChannelException e) {
				env.callNo++;
				env.blockcallNo++;
			}
		} else if(event.getType() == CallEvent.TYPE.HANDOVER) {
			//first remove the user from it's current channel
			env.getStation(user.getCurrentBaseStation()).removeUser(user);
			//then allocate to the next basestation
			//if user is currently at the last station, then just terminate it
			if(user.getCurrentBaseStation()<19) {
				try {
					env.getStation(user.getCurrentBaseStation()+1).allocateUser(user, false);
					//need to change user fields
					user.setCurrentBaseStation(user.getCurrentBaseStation()+1);
					user.setCurrentLocation(0.);
					//Then add the most recent handover or terminate event to the eventqueue
					handoverTime = 2. / user.getVelocity() * 3600. + event.getTime();
					terminateTime = user.getArrivalTime() + user.getCallDuration();
					if( terminateTime < handoverTime ) {
						eventQueue.add(new CallEvent(CallEvent.TYPE.TERMINATION, user, terminateTime));
					}else {
						eventQueue.add(new CallEvent(CallEvent.TYPE.HANDOVER, user, handoverTime));
					}
				} catch (NoFreeChannelException e) {
					env.dropcallNo++;
				}
			}
		} else if(event.getType() == CallEvent.TYPE.TERMINATION) {
			//just remove the user from it's current channel
			env.getStation(user.getCurrentBaseStation()).removeUser(user);
		}
		//advance the environment time
		env.setCurrentTime(event.getTime());
	}
}
