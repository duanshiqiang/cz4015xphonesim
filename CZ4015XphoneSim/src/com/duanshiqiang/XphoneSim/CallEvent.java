package com.duanshiqiang.XphoneSim;

import java.util.Comparator;

public class CallEvent {
	public enum TYPE{
		DISPATCHE,
		TERMINATION,
		HANDOVER;
	}
	private TYPE type;
	private CallOnCar user;
	private double time;
	
	public CallEvent(TYPE type, CallOnCar user, double time) {
		this.type = type;
		this.user = user;
		this.time = time;
	}
	
	public double getTime() {
		return this.time;
	}
	
	public TYPE getType() {
		return type;
	}

	public CallOnCar getUser() {
		return user;
	}

	public static class CallEventTimeComparator implements Comparator<CallEvent> {
		@Override
		public int compare(CallEvent o1, CallEvent o2) {
			return Double.compare(o1.getTime(), o2.getTime());
		}
	}
}
