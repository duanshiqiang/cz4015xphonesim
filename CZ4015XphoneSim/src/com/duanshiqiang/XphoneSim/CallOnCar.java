package com.duanshiqiang.XphoneSim;

public class CallOnCar {
	private double arrivalTime;
	private int initialBaseStationNo;
	private double callDuration;
	private double velocity;
	/**
	 * It's the distance from the beginning of the base station
	 */
	private double initialLocation;
	
	private int currentBaseStation;
	private double currentLocation;
	
	public CallOnCar(double arrivalTime, int initialBaseStationNo,
			double callDuration, double velocity, double initialLocation) {
		this.arrivalTime = arrivalTime;
		this.initialBaseStationNo = initialBaseStationNo;
		this.callDuration = callDuration;
		this.velocity = velocity;
		this.initialLocation = initialLocation;
		
		this.currentBaseStation = initialBaseStationNo;
		this.currentLocation = initialLocation;
	}
	
	public double getArrivalTime() {
		return arrivalTime;
	}
	public int getInitialBaseStationNo() {
		return initialBaseStationNo;
	}
	public double getCallDuration() {
		return callDuration;
	}
	public double getVelocity() {
		return velocity;
	}
	
	public double getInitialLocation() {
		return initialLocation;
	}
	
	public int getCurrentBaseStation() {
		return currentBaseStation;
	}

	public void setCurrentBaseStation(int currentBaseStation) {
		this.currentBaseStation = currentBaseStation;
	}

	public double getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(double currentLocation) {
		this.currentLocation = currentLocation;
	}

	@Override
	public boolean equals(Object obj) {
		CallOnCar o = (CallOnCar)obj;
		return o.arrivalTime == this.arrivalTime &&
			o.initialBaseStationNo == this.initialBaseStationNo &&
			o.callDuration == this.callDuration &&
			o.velocity == this.velocity &&
			o.initialLocation == this.initialLocation;
	}
}
