package com.duanshiqiang.XphoneSim;

public class Channel {
	private CallOnCar user;
	private boolean isFree;
	
	public Channel() {
		this.user = null;
		this.isFree = true;
	}
	
	public boolean isFree() {
		return isFree;
	}
	
	public void setFree(boolean isFree) {
		this.isFree = isFree;
	}

	public CallOnCar getUser() {
		return user;
	}

	public void setUser(CallOnCar user) {
		this.user = user;
	}
	
}
