package com.duanshiqiang.XphoneSim;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * This is to simulate the road
 * In this project, the highway containing 20 Phone base station
 * @author Duan Shiqiang
 */
public class RoadEnv {
	private ArrayList<BaseStation> stations;
	private double currentTime;
	
	public int callNo=0;
	public int dropcallNo=0;
	public int blockcallNo=0;
	
	private PriorityQueue<CallEvent> callEventQueue;
	
	public RoadEnv(int baseStationReservedChannleNo) {
		this.stations = new ArrayList<BaseStation>();
		for(int i=0; i<20; i++) {
			this.stations.add(new BaseStation(baseStationReservedChannleNo));
		}
		this.currentTime = 0.;
		this.callEventQueue = new PriorityQueue<CallEvent>(50, new CallEvent.CallEventTimeComparator());
	}
	
	public RoadEnv(int[] baseStationReservedChannleNo) {
		
	}
	
	/**
	 * Load the excel data given by our lecturer
	 * @throws IOException 
	 * @throws InvalidFormatException 
	 */
	public void loadExcelData(File xmlFile) throws InvalidFormatException, IOException {
		HSSFWorkbook wb = (HSSFWorkbook) WorkbookFactory.create(xmlFile);
		HSSFSheet sheet = wb.getSheetAt(0);
		int rows = sheet.getPhysicalNumberOfRows();
		Row tmp;
		Random random = new Random();
		double range;
		CallOnCar user;
		for(int i=1; i<rows; i++){
			tmp = sheet.getRow(i);
			range = random.nextDouble()*2;
			user = new CallOnCar(
					tmp.getCell(1).getNumericCellValue(),
					(int)tmp.getCell(2).getNumericCellValue()-1,
					tmp.getCell(3).getNumericCellValue(),
					tmp.getCell(4).getNumericCellValue(),
					range
					);
			this.callEventQueue.add(
				new CallEvent(CallEvent.TYPE.DISPATCHE, user, user.getArrivalTime()) );
		}
	}
	
	public void runsim(CallEventHandler callEventHandler) {
		while(this.callEventQueue.peek()!=null) {
			this.step(callEventHandler);
		}
	}
	
	public boolean step(CallEventHandler callEventHandler) {
		CallEvent event = null;
		if(this.callEventQueue.peek()!=null) {
			event = this.callEventQueue.poll();
			callEventHandler.handle(this, this.callEventQueue, event);
			return true;
		}else {
			return false;
		}
	}
	
	public double getChannelUsagePercentage() {
		int cnInUse = 0;
		for(BaseStation bs: this.stations) {
			for(Channel cn: bs.getChannels()) {
				if(!cn.isFree()) {
					cnInUse++;
				}
			}
		}
		return (double)cnInUse / 200.;
	}
	public BaseStation getStation(int index) {
		return this.stations.get(index);
	}

	public double getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(double currentTime) {
		this.currentTime = currentTime;
	}
	
}